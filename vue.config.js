const webpack = require('webpack');

module.exports = {
    configureWebpack: {
        plugins: [
            new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en|fr/)
        ],
    },
    assetsDir: 'public',
    devServer: {
    host: 'localhost', // can be overwritten by process.env.HOST
    overlay: {
      warnings: true,
      errors: true,
    },
  },
}
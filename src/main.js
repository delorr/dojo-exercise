import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { store } from './store'

// Store
// import store from './store';

// Style
import './scss/layout.scss';

const app = createApp(App).use(router).use(store);
app.mount('#app');
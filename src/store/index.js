import { createStore } from 'vuex'


export const store = createStore({
  state () {
    return {
      takas: [
          "Anagram",
          "Game Of Life",
          "FooBarQix"
      ]
    }
  },
  getters: {
      getAllTakas: state => {
          return state.takas
      }
  }
})
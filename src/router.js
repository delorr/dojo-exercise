import { createRouter, createWebHistory } from 'vue-router';

import Dashboard from './views/Dashboard.vue';

// Exercise
import Anagram from './views/Anagram/Anagram.vue';
import Foobarqix from './views/Foobarqix/Foobarqix.vue';
import GameOfLife from './views/GameOfLife/GameOfLife.vue';


const routes = [
    {
        path: '/',
        component: Dashboard
    },
    {
        path: '/anagram',
        component: Anagram
    },
    {
        path: '/foobarqix',
        component: Foobarqix
    },
    {
        path: '/GameofLife',
        component: GameOfLife
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
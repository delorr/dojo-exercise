// sample_specs.js created with Cypress

describe('My First Test', () => {
    it('Visits the app roots url', () => {
      cy.visit('/');
    });
  });
describe('FooBarQix Test', () => {
    it('Test 101 number', () => {
        cy.visit('/foobarqix');

        cy.get('input[id="foo"]')
        .type(101)

        cy.get('button[id="mutate"]').click()

        cy.get('span[id="result"]').should(($div) => {
          expect($div.text().trim()).equal('1*1');
        });
    });
});